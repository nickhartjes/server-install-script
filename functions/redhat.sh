####################################################################
# Vars
####################################################################
echo "redhat functions were loaded"
apache_conf='/etc/httpd/conf/httpd.conf'
apache_user='apache'
ssh_service='/etc/init.d/sshd'
mysql_service='mysqld'
apache_service='/etc/init.d/httpd'


if [ "${DIST}" = "CentOS" ] ; then
	# Recommended to CENTOS 
	userProfile='export PS1="\[\e[0;36m\]\u\[\e[1;35m\]@\H \[\033[0;36m\] \w\[\e[0m\]$ "'
	rootProfile='export PS1="\[\e[1;31m\]\u\[\e[1;35m\]@\H \[\033[0;36m\] \w\[\e[0m\]$ "'
else
	# Recommended to REDHAT
	userProfile='export PS1="\[\e[0;36m\]\u\[\e[1;31m\]@\H \[\033[0;36m\] \w\[\e[0m\]$ "'
	rootProfile='export PS1="\[\e[1;31m\]\u\[\e[1;31m\]@\H \[\033[0;36m\] \w\[\e[0m\]$ "'
fi
####################################################################
# Create USER
####################################################################
createUser(){
	if [ ! `whoami` = "root" ]; then 
		echo -e "$red Error: You must to be ROOT user to run this function $endColor"
		return
	fi
	echo -e "$cyan============================ Creating a user $user... =============================$endColor"

	test_user=`id -u $user`;

	if [ "$test_user" = "550" ]; then 
		echo -e "$cyan##### REMOVING PREVIUOS USER ENTRY #####$endColor"
		userdel -r $user
	fi

	echo -e "$cyan##### Add user $user #####$endColor"
	adduser $user -d /home/$user -u 550 -G wheel
	echo "$passwd" | passwd --stdin $user

	echo -e "$cyan##### Add wheel group to sudo #####$endColor"
	sed '/^#.*%wheel\tALL=(ALL)\tALL.*/ s/^#//' /etc/sudoers > tmp
	cat tmp > /etc/sudoers
	echo -e "$cyan==================== User $user created successfully ====================$endColor"
}
####################################################################
# Profile USER
####################################################################
profileUser(){
	echo $userProfile > tmp
	cat tmp >> /home/$user/.bash_profile
	source /home/$user/.bash_profile
	echo -e "$cyan==================== Bash Profile to User $user created ====================$endColor"
	echo $rootProfile > tmp
	cat tmp >> /root/.bash_profile
	source /root/.bash_profile
	echo -e "$cyan==================== Bash Profile to User root created ====================$endColor"
}
####################################################################
# Update and Install Apache, PHP, MySQL, Django, Subversion, TRAC
####################################################################
updateInstall(){
	echo -e "$cyan======= Updating and Installing Apache, PHP, MySQL, Django, Subversion ======$endColor"

	echo -e "$cyan##### Updating Operating System... #####$endColor" 
	yum clean all
	yum -y update
	echo -e "$cyan================ System Updated successfully ================$endColor"

	echo -e "$cyan================ Packages Installed successfully ================$endColor"
}
