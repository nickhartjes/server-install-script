
####################################################################
# Configurating SSH and IPTABLES
####################################################################
sshIptables(){
	echo -e "$cyan====================== Configurating SSH and IPTABLES  =========================$endColor"

	if [ ! -f /etc/ssh/sshd_config.orig ]; then
		cp /etc/ssh/sshd_config /etc/ssh/sshd_config.orig
	fi
	echo -e "$cyan##### SSH: Add Port $port #####$endColor"

	currentPort=$(cat  /etc/ssh/sshd_config | grep "Port" | sort -u)
	echo "Currentport is $currentPort"

	sed "/^Port 22.*/ s/^Port 22/Port $port/" /etc/ssh/sshd_config > tmp
	sudo cat tmp > /etc/ssh/sshd_config
	sed '/^PermitRootLogin yes.*/ s/^PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config > tmp
	cat tmp > /etc/ssh/sshd_config
	sed '/^X11Forwarding yes.*/ s/^X11Forwarding yes/X11Forwarding no/' /etc/ssh/sshd_config > tmp
	cat tmp > /etc/ssh/sshd_config

	echo -e "$cyan##### IPTABLES RULES #####$endColor"
	iptables -F
	#  Allows all loopback (lo0) traffic and drop all traffic to 127/8 that doesn't use lo0
	iptables -A INPUT -i lo -j ACCEPT
	iptables -A INPUT ! -i lo -d 127.0.0.0/8 -j REJECT
	#  Accepts all established inbound connections
	iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
	#  Allows all outbound traffic
	iptables -A OUTPUT -j ACCEPT
	# Allows HTTP and HTTPS connections from anywhere (the normal ports for websites)
	iptables -A INPUT -p tcp --dport 80 -j ACCEPT
	iptables -A INPUT -p tcp --dport 443 -j ACCEPT
	iptables -A INPUT -p tcp --dport 21 -j ACCEPT 
	#  Allows SSH connections
	iptables -A INPUT -p tcp -m state --state NEW --dport $port -j ACCEPT
	# Add VNC Port 5900
	# iptables -A INPUT -p tcp -m state --state NEW --dport 5900 -j ACCEPT
	# Allow ping
	iptables -A INPUT -p icmp -m icmp --icmp-type 8 -j ACCEPT

	# log iptables denied calls
	iptables -A INPUT -m limit --limit 5/min -j LOG --log-prefix "iptables denied: " --log-level 7

	# Reject all other inbound - default deny unless explicitly allowed policy
	iptables -A INPUT -j REJECT
	iptables -A FORWARD -j REJECT

	echo -e "$cyan##### IPTABLES & SSH RELOAD #####$endColor"
	#sudo /etc/init.d/iptables save
	/sbin/iptables-save

	sudo service  reload
	echo -e "$cyan=============== SSH & IPTABLES setted successfully ===============$endColor"
}



