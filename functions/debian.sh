####################################################################
# Vars
####################################################################
echo "debian functions were loaded"
ssh_service='/etc/init.d/ssh'

userProfile='export PS1="\[\e[0;36m\]\u\[\e[1;33m\]@\H \[\033[0;36m\] \w\[\e[0m\]$ "'
rootProfile='export PS1="\[\e[1;31m\]\u\[\e[1;33m\]@\H \[\033[0;36m\] \w\[\e[0m\]$ "'

####################################################################
# Create USER
####################################################################
createUser(){
	if [ ! `whoami` = "root" ]; then 
		echo -e "$red Error: You must to be ROOT user to run this function $endColor"
		return
	fi
	echo -e "$cyan============================ Creating a user $user... =============================$endColor"

	apt-get install whois
	apt-get install mkpasswd
        echo -e "$cyan##### Deleting user $user #####$endColor"
	userdel -r $user

	addgroup admin

	echo -e "$cyan##### Add user $user #####$endColor"
	adduser --system $user  --uid 550  --ingroup admin
	usermod -p `mkpasswd $passwd` $user

	echo -e "$cyan##### Add wheel group to sudo #####$endColor"
	sed '/^#.*%admin\tALL=(ALL)\tALL.*/ s/^#//' /etc/sudoers > tmp
	cat tmp > /etc/sudoers
	echo -e "$cyan==================== User $user created successfully ====================$endColor"
}
####################################################################
# Profile USER
####################################################################
profileUser(){
	echo $userProfile > tmp
	cat tmp >> /home/$user/.bashrc
	source /home/$user/.bashrc
	echo -e "$cyan==================== Bash Profile to User $user created ====================$endColor"
	echo $rootProfile > tmp
	cat tmp >> /root/.bashrc
	source /root/.bashrc
	echo -e "$cyan==================== Bash Profile to User root created ====================$endColor"
}
####################################################################
# Update and Install SSH, MC, VIM, FAIL2BAN
####################################################################
updateInstall(){
	echo -e "$cyan======= Install SSH, MC, VIM, FAIL2BAN ======$endColor"

	echo -e "$cyan##### Updating Operating System... #####$endColor" 
	apt-get -y update
	apt-get -y upgrade
	echo -e "$cyan================ System Updated successfully ================$endColor"

	apt-get -y install openssh-server
	apt-get -y install mc
	apt-get -y install vim

	apt-get -y install fail2ban
	sed "/^destemail = root@localhost.*/ s/^destemail = root@localhost/destemail = $mailAdmin/" /etc/fail2ban/jail.conf > tmp
	echo $tmp
	sudo cat tmp > /etc/fail2ban/jail.conf

	sed "/^action = %(action_)s.*/ s/^action = %(action_)s/action = %(action_mwl)s/" /etc/fail2ban/jail.conf > tmp
	echo $tmp
	sudo cat tmp > /etc/fail2ban/jail.conf
	sudo service fail2ban restart

	echo -e "$cyan================ Packages Installed successfully ================$endColor"
}
####################################################################
# Install internal IP for Transip private network
####################################################################
updateInternalIp(){
	echo -e "$cyan======= Install internal IP for Transip private network ======$endColor"

	echo "

auto eth1
iface eth1 inet static
    address $internalIp
    netmask 255.255.255.0" >> /etc/network/interfaces

    sudo service networking restart

	echo -e "$cyan================ Packages Installed successfully ================$endColor"
}

####################################################################
# Install ElasticSearchCluster
####################################################################
installElasticSearchCluster(){
	echo -e "$cyan======= Install Elastic Search Cluster ======$endColor"

	sudo apt-get install openjdk-7-jre-headless -y
	wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-$elasticSearchVersion.deb
	sudo dpkg -i elasticsearch-$elasticSearchVersion.deb

	curl -L http://github.com/elasticsearch/elasticsearch-servicewrapper/tarball/master | tar -xz
	sudo mv *servicewrapper*/service /usr/local/share/elasticsearch/bin/
	rm -Rf *servicewrapper*
	sudo /usr/local/share/elasticsearch/bin/service/elasticsearch install
	sudo ln -s `readlink -f /usr/local/share/elasticsearch/bin/service/elasticsearch` /usr/local/bin/rcelasticsearch

	sed "/^# cluster.name: elasticsearch.*/ s/^# cluster.name: elasticsearch/cluster.name: $elasticSearchClusterName/" /etc/elasticsearch/elasticsearch.yml > tmp
	sudo cat tmp > /etc/elasticsearch/elasticsearch.yml

	sed '/^# node.name: "Franz Kafka".*/ s/^# node.name: "Franz Kafka"/node.name: "$elasticSearchNodeName"/' /etc/elasticsearch/elasticsearch.yml > tmp
	sudo cat tmp > /etc/elasticsearch/elasticsearch.yml

	sed "/^# node.data: true.*/ s/^# node.data: true/node.data: $elasticSearchData/" /etc/elasticsearch/elasticsearch.yml > tmp
	sudo cat tmp > /etc/elasticsearch/elasticsearch.yml
	 
	sudo service elasticsearch restart

	

	echo -e "$cyan================ Packages Installed successfully ================$endColor"
}
